export default {
  name: "requestHandler",
  data() {
    return {
      isLoading: false,
      displayErrorToast: true,
      errorMessage: null
    };
  },
  methods: {
    handleErrors(error) {
      if (error.message) {
        this.errorMessage = error.message;
      }

      if (this.displayErrorToast)
        this.$buefy.toast.open({
          message: "An error occurred.",
          type: "is-danger"
        });
    },
    handleRequest(requestMethod) {
      let $scope = this;
      $scope.isLoading = true;

      requestMethod()
        .then(() => {
          $scope.errors = {};
          $scope.isLoading = false;
        })
        .catch(errors => {
          $scope.handleErrors(errors);
          $scope.isLoading = false;
        });
    }
  }
};
