import { firebaseAction, firestoreAction, vuexfireMutations } from "vuexfire";
import Vue from "vue";
import Vuex from "vuex";
import router from "@/router";
import { ToastProgrammatic as Toast } from "buefy";
import { $firestore, $realtimeDb } from "@/firebase";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    isLoading: true,
    user: null,
    resultsLimit: 10,
    firestorePage: 1,
    firestoreData: [],
    realtimePage: 1,
    realtimeData: []
  },
  mutations: {
    setLoading(state, payload) {
      state.isLoading = payload;
    },
    setUser(state, payload) {
      state.user = payload;
    },
    incrementFirestorePage(state, payload) {
      state.firestorePage += payload;
    },
    setFirestorePage(state, payload) {
      state.firestorePage = payload;
    },
    incrementRealtimePage(state, payload) {
      state.realtimePage += payload;
    },
    setRealtimePage(state, payload) {
      state.realtimePage = payload;
    },
    ...vuexfireMutations
  },
  actions: {
    handleAuthChange(context, user) {
      if (user) {
        context.commit("setUser", {
          photoURL: user.photoURL,
          displayName: user.displayName,
          email: user.email
        });

        if (router.currentRoute.fullPath === "/")
          router.push("/dashboard/firestore");

        Toast.open({
          type: "is-success",
          message: "You have been logged in."
        });
      } else {
        let existingUser = context.state.user !== null;

        context.commit("setUser", user);

        if (
          router.currentRoute.matched.some(({ path }) => path === "/dashboard")
        )
          router.push("/");

        if (existingUser) {
          Toast.open({
            type: "is-success",
            message: "You have been logged out."
          });
        }
      }

      context.commit("setLoading", false);
    },
    bindFirestoreData: firestoreAction((context, payload) => {
      context.commit("setLoading", true);

      let reference = $firestore.collection("loads");

      if (payload && payload.id) {
        context.commit("setFirestorePage", 1);
        reference = reference.where("id", "==", payload.id).limit(1);
      }

      if (payload && payload.paginate && payload.paginate === 1) {
        reference = reference
          .orderBy("id", "asc")
          .startAfter(
            context.state.firestoreData[context.state.firestoreData.length - 1]
              .id
          )
          .limit(context.state.resultsLimit);
        context.commit("incrementFirestorePage", 1);
      } else if (payload && payload.paginate && payload.paginate === -1) {
        reference = reference
          .orderBy("id", "asc")
          .endBefore(context.state.firestoreData[0].id)
          .limitToLast(context.state.resultsLimit);
        context.commit("incrementFirestorePage", -1);
      } else if (!payload || !payload.id) {
        context.commit("setFirestorePage", 1);
        reference = reference
          .limit(context.state.resultsLimit)
          .orderBy("id", "asc");
      }

      return context
        .bindFirestoreRef("firestoreData", reference, { wait: true })
        .then(() => {
          context.commit("setLoading", false);
        })
        .catch(e => {
          router.push("/");
          console.log(e);
          context.commit("setLoading", false);
        });
    }),
    bindRealtimeData: firebaseAction((context, payload) => {
      context.commit("setLoading", true);

      let reference = $realtimeDb.ref("loads").orderByKey();

      if (payload && payload.id) {
        reference = reference.equalTo(payload.id);
        context.commit("setRealtimePage", 1);
      }

      if (payload && payload.paginate && payload.paginate === 1) {
        reference = reference
          .limitToFirst(context.state.resultsLimit)
          .startAt(
            context.state.realtimeData[context.state.realtimeData.length - 1].id
          );
        context.commit("incrementRealtimePage", 1);
      } else if (payload && payload.paginate && payload.paginate === -1) {
        reference = reference
          .limitToLast(context.state.resultsLimit)
          .endAt(context.state.realtimeData[0].id);
        context.commit("incrementRealtimePage", -1);
      } else if (!payload || !payload.id) {
        context.commit("setRealtimePage", 1);
        reference = reference.limitToFirst(context.state.resultsLimit);
      }

      return context
        .bindFirebaseRef("realtimeData", reference, { wait: true })
        .then(() => {
          context.commit("setLoading", false);
        })
        .catch(() => {
          router.push("/");
          context.commit("setLoading", false);
        });
    }),
    unbindFirestoreData: firestoreAction(context => {
      context.commit("setFirestorePage", 1);
      context.unbindFirestoreRef("firestoreData");
    }),
    unbindRealtimeData: firebaseAction(context => {
      context.commit("setRealtimePage", 1);
      context.unbindFirebaseRef("realtimeData");
    })
  },
  modules: {}
});
