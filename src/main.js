import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import Buefy from "buefy";
import "buefy/dist/buefy.css";
import { $auth } from "@/firebase";

Vue.config.productionTip = false;
Vue.use(Buefy, {
  defaultIconPack: "fal"
});

new Vue({
  router,
  store,
  created() {
    $auth.onAuthStateChanged(user => {
      store.dispatch("handleAuthChange", user);
    });
  },
  render: h => h(App)
}).$mount("#app");
