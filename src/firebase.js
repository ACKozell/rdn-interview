import firebase from "firebase/app";
import "firebase/auth";
import "firebase/firestore";
import "firebase/database";
import config from "../firebase.config.js";

const firebaseApp = firebase.initializeApp(config);

export { firebase };
export const $auth = firebaseApp.auth();
export const $firestore = firebaseApp.firestore();
export const $realtimeDb = firebaseApp.database();
