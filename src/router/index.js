import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import Dashboard from "@/layouts/Dashboard";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home
  },
  {
    path: "/dashboard",
    component: Dashboard,
    children: [
      {
        path: "",
        redirect: "firestore"
      },
      {
        path: "firestore",
        name: "Firestore",
        component: () =>
          import(/* webpackChunkName: "dashboard" */ "../views/Firestore.vue")
      },
      {
        path: "realtime-db",
        name: "Realtime Database",
        component: () =>
          import(
            /* webpackChunkName: "dashboard" */ "../views/RealtimeDatabase.vue"
          )
      }
    ]
  },
  {
    path: "*",
    name: "404",
    component: () =>
      import(/* webpackChunkName: "errors" */ "../views/Error404.vue")
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
